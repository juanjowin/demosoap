package com.webServiceSoap.dto;

public class InvoiceDetailDto {
	
		private int iddetail;
		private int idinvoice;
		private int idproduct;
		private int quantity;
		private double price;
		
		
		public InvoiceDetailDto() {

		}
		
		public InvoiceDetailDto(int idinvoice, int idproduct, int quantity, double price) {
			this.idinvoice = idinvoice;
			this.idproduct = idproduct;
			this.quantity = quantity;
			this.price = price;
		}
		
		public int getIddetail() {
			return iddetail;
		}
		public void setIddetail(int iddetail) {
			this.iddetail = iddetail;
		}
		public int getIdinvoice() {
			return idinvoice;
		}
		public void setIdinvoice(int idinvoice) {
			this.idinvoice = idinvoice;
		}
		public int getIdproduct() {
			return idproduct;
		}
		public void setIdproduct(int idproduct) {
			this.idproduct = idproduct;
		}
		public int getQuantity() {
			return quantity;
		}
		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}
		public double getPrice() {
			return price;
		}
		public void setPrice(double price) {
			this.price = price;
		}

		@Override
		public String toString() {
			return "InvoiceDetailDto [iddetail=" + iddetail + ", idinvoice=" + idinvoice + ", idproduct=" + idproduct
					+ ", quantity=" + quantity + ", price=" + price + "]";
		}
		
		
		

}
