package com.webServiceSoap.dto;



import java.util.Date;
import java.util.List;

public class BillingDto{
		
	private int idinvocie;
	private int idclient;
	private int numinvoice;
	private double total;
	private Date creationdate;
	
	List<InvoiceDetailDto> invoiceDetailDtos;
	
	public BillingDto() {
	}
	public BillingDto(int idclient, int numinvoice, double total,Date creationdate) {
		
		this.idclient = idclient;
		this.numinvoice = numinvoice;
		this.total = total;
		this.creationdate=creationdate;	
	}

	public int getIdinvocie() {
		return idinvocie;
	}
	public void setIdinvocie(int idinvocie) {
		this.idinvocie = idinvocie;
	}
	public int getIdclient() {
		return idclient;
	}
	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}
	public int getNuminvoice() {
		return numinvoice;
	}
	public void setNuminvoice(int numinvoice) {
		this.numinvoice = numinvoice;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public Date getCreationdate() {
		return creationdate;
	}
	public void setCreationdate(Date creationdate) {
		this.creationdate = creationdate;
	}
	
	@Override
	public String toString() {
		return "BillingDto [idinvocie=" + idinvocie + ", idclient=" + idclient + ", numinvoice=" + numinvoice
				+ ", total=" + total + ", creationdate=" + creationdate + "]";
	}
	
	
	
	
	
}
