package com.webServiceSoap.endpoint;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import com.webServiceSoap.dao.InvoiceDetailDao;
import com.webServiceSoap.dto.BillingDto;
import com.webServiceSoap.dto.InvoiceDetailDto;
import com.webServiceSoap.service.BillingService;
import com.webServiceSoap.service.InvoiceDetailService;
import com.webservice.soap_ws.DeleteInvoiceDetailRequest;
import com.webservice.soap_ws.DeleteInvoiceDetailResponse;
import com.webservice.soap_ws.DeleteInvoiceRequest;
import com.webservice.soap_ws.DeleteInvoiceResponse;
import com.webservice.soap_ws.GetAllInvoiceRequest;
import com.webservice.soap_ws.GetAllInvoiceResponse;
import com.webservice.soap_ws.GetInvoiceByNumberRequest;
import com.webservice.soap_ws.GetInvoiceByNumberResponse;
import com.webservice.soap_ws.InvoiceType;
import com.webservice.soap_ws.InvoicedetailType;
import com.webservice.soap_ws.SaveInvoiceRequest;
import com.webservice.soap_ws.SaveInvoiceRequest2;
import com.webservice.soap_ws.SaveInvoiceResponse;
import com.webservice.soap_ws.ServiceStatus;
import com.webservice.soap_ws.UpdateInvoiceRequest;
import com.webservice.soap_ws.UpdateInvoiceResponse;


@Endpoint
public class BillignEndpoint {

	public static final String NAMESPACE_URI ="http://www.webServiceSoap/invoice";
	
	@Autowired
	private BillingService billingService;
	@Autowired
	private InvoiceDetailService invoiceDetailService;
	
	public BillignEndpoint() {
		
	}

	@Autowired
	public BillignEndpoint(BillingService billingService) {
		this.billingService = billingService;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getInvoiceByNumberRequest")
	@ResponsePayload
	public 	GetInvoiceByNumberResponse getInvoiceByNumber(@RequestPayload GetInvoiceByNumberRequest request) {
			GetInvoiceByNumberResponse response=new GetInvoiceByNumberResponse();
			BillingDto invoiceDto= billingService.findByInvoiceNumber(request.getNuminvoice());
			ServiceStatus serviceStatus=new ServiceStatus();
			
			List<InvoicedetailType> InvoicedetailTypeList=new ArrayList<InvoicedetailType>();
			
	
			
		
			List<InvoiceDetailDto> detailList=invoiceDetailService.findById(request.getIddetail());
	
				for(InvoiceDetailDto item: detailList) {
						InvoicedetailType invoicedetailType=new InvoicedetailType();
						BeanUtils.copyProperties(item, invoicedetailType);
						InvoicedetailTypeList.add(invoicedetailType);
						response.setInvoicedetailType(invoicedetailType);
					}
					
				InvoiceType invoiceType= new InvoiceType();
				BeanUtils.copyProperties(invoiceDto, invoiceType);
			
			response.setInvoiceType(invoiceType);
			response.setServiceStatus(serviceStatus);
			return response;
	}
	
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllInvoiceRequest")
	@ResponsePayload
	public 	GetAllInvoiceResponse getAllInvoice (@RequestPayload GetAllInvoiceRequest request) {
			GetAllInvoiceResponse response=new GetAllInvoiceResponse();
			List<InvoiceType> invoiceTypelist= new ArrayList<InvoiceType>();
			List<BillingDto> biList= billingService.findAll();
			
			for (BillingDto item: biList) {
				InvoiceType invoiceType=new InvoiceType();
				BeanUtils.copyProperties(item, invoiceType);
				invoiceTypelist.add(invoiceType);
			}
			
			response.getInvoiceType().addAll(invoiceTypelist);
			return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "saveInvoiceRequest")
	@ResponsePayload
	public 	SaveInvoiceResponse saveInvoice(@RequestPayload SaveInvoiceRequest request) {
			SaveInvoiceResponse response=new SaveInvoiceResponse();
			InvoiceType newinvoiceType=new InvoiceType();
			ServiceStatus serviceStatus=new ServiceStatus();
			
		
				BillingDto newbillingDto= new BillingDto();
				
				
				billingService.save(newbillingDto);
				 

				BeanUtils.copyProperties(newbillingDto, newinvoiceType);
				

			response.setInvoiceType(newinvoiceType);
			response.setServiceStatus(serviceStatus);
			return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateInvoiceRequest")
	@ResponsePayload
	public 	UpdateInvoiceResponse updateInvoice (@RequestPayload UpdateInvoiceRequest request) {			
			UpdateInvoiceResponse response=new UpdateInvoiceResponse();
			ServiceStatus serviceStatus=new ServiceStatus();
			
			BillingDto billingDtoBD=billingService.findByInvoiceNumber(request.getNuminvoice());
				
					billingDtoBD.setIdclient(request.getIdclient());
					billingDtoBD.setNuminvoice(request.getNuminvoice());
					billingDtoBD.setTotal(request.getTotal());
					billingDtoBD.setCreationdate(request.getCreationdate().toGregorianCalendar().getTime());				
					
					billingService.update(billingDtoBD);
				
			response.setServiceStatus(serviceStatus);
			return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteInvoiceRequest") 
	@ResponsePayload
	public DeleteInvoiceResponse deleteInvoice(@RequestPayload DeleteInvoiceRequest request) {
		   DeleteInvoiceResponse response=new DeleteInvoiceResponse();
		   ServiceStatus serviceStatus=new ServiceStatus();
		   
		   	billingService.delete(request.getNuminvoice());

		   	response.setServiceStatus(serviceStatus);
		   	return response;
	}
	
	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteInvoiceDetailRequest") 
	@ResponsePayload
	public DeleteInvoiceDetailResponse deleteInvoiceDetailRequest(@RequestPayload DeleteInvoiceDetailRequest request) {
		DeleteInvoiceDetailResponse response=new DeleteInvoiceDetailResponse();
		   ServiceStatus serviceStatus=new ServiceStatus();
		   
		   invoiceDetailService.delete(request.getIddetail());

		   	response.setServiceStatus(serviceStatus);
		   	return response;
	}
}










