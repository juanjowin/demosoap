package com.webServiceSoap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootWebServiceSoapApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootWebServiceSoapApplication.class, args);
	}

}
