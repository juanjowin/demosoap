package com.webServiceSoap.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.springframework.jdbc.core.RowMapper;

import com.webServiceSoap.dto.BillingDto;

public class BillingDtolRowMapper implements RowMapper<BillingDto>{

	@Override
	public BillingDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		
	
		BillingDto billingDto=new BillingDto();
		
		billingDto.setIdinvocie(rs.getInt("idinvoice"));
		billingDto.setIdclient(rs.getInt("idclient"));
		billingDto.setNuminvoice(rs.getInt("numinvoice"));
		billingDto.setTotal(rs.getDouble("total"));
		billingDto.setCreationdate(rs.getDate("creationdate"));
		
		return billingDto;
	}

}
