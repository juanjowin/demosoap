package com.webServiceSoap.dao;

import java.util.List;

import com.webServiceSoap.dto.BillingDto;
import com.webServiceSoap.dto.InvoiceDetailDto;

public interface InvoiceDetailDao {
	public boolean save(InvoiceDetailDto invoiceDetailDto);

	public List<InvoiceDetailDto> findById(int iddetail);
	public boolean delete(int iddetail);
	public List<InvoiceDetailDto> findAll();
		
}
