package com.webServiceSoap.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.webServiceSoap.dto.BillingDto;
import com.webServiceSoap.dto.InvoiceDetailDto;

@Repository
public class InvoiceDetailDaoImpl implements InvoiceDetailDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public boolean save(InvoiceDetailDto invoiceDetailDto) {
		return jdbcTemplate.update("insert into invoicedetail(idinvoice,idproduct,quantity,price) values(?,?,?,?)",
				invoiceDetailDto.getIdinvoice(),invoiceDetailDto.getIdproduct(),invoiceDetailDto.getQuantity(),invoiceDetailDto.getPrice())==1;
	}

	@Override
	public List<InvoiceDetailDto> findAll() {
		return jdbcTemplate.query("select * from invoicedetail" , new InvoiceDetailDtolRowMapper());
	}

	@Override
	public boolean delete(int iddetail) {
		 return jdbcTemplate.update("delete from invoicedetail where iddetail=?",iddetail)==1;
	}

	@Override
	public List<InvoiceDetailDto> findById(int iddetail) {
		return jdbcTemplate.query( "select * from invoicedetail where iddetail=?", new InvoiceDetailDtolRowMapper(), iddetail);
	}

}
