package com.webServiceSoap.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import com.webServiceSoap.dto.BillingDto;
import com.webServiceSoap.dto.InvoiceDetailDto;

public class InvoiceDetailDtolRowMapper  implements RowMapper<InvoiceDetailDto> {

	@Override
	public InvoiceDetailDto mapRow(ResultSet rs, int rowNum) throws SQLException {

		InvoiceDetailDto invoiceDetailDto=new InvoiceDetailDto();
		
		invoiceDetailDto.setIdinvoice(rs.getInt("idinvoice"));
		invoiceDetailDto.setIdproduct(rs.getInt("idproduct"));
		invoiceDetailDto.setQuantity(rs.getInt("quantity"));
		invoiceDetailDto.setPrice(rs.getDouble("price"));
		
		
		return invoiceDetailDto;
	}

}
