package com.webServiceSoap.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.webServiceSoap.dto.BillingDto;

@Repository
public class BillingDaoImpl implements BillingDao{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public BillingDto findByInvoiceNumber(int numinvoice) {
		BillingDto num=null;
		try {
			num=jdbcTemplate.queryForObject( "select * from invoice where numinvoice=?", new BillingDtolRowMapper(), numinvoice); 
		}catch (EmptyResultDataAccessException e) {
			return null;
		}	
		 return num;
	}
	
	@Override
	public BillingDto save(BillingDto billingDto) {	
		
		jdbcTemplate.update("insert into invoice(idclient,numinvoice,total,creationdate) values(?,?,?,?)",
					billingDto.getIdclient(),billingDto.getNuminvoice(),billingDto.getTotal(),billingDto.getCreationdate());
		
		return billingDto;
	}
	
	@Override
	public boolean update(BillingDto billingDto) {
		return jdbcTemplate.update("update invoice set  idclient=?, numinvoice=?, total=?, creationdate=? where numinvoice=?",
				billingDto.getIdclient(),billingDto.getNuminvoice(),billingDto.getTotal(),billingDto.getCreationdate(),billingDto.getNuminvoice())==1;
	}

	@Override
	public boolean delete(int numinvoice) {
		 return jdbcTemplate.update("delete from invoice where numinvoice=?",numinvoice)==1;
		
	}

	@Override
	public List<BillingDto> findAll() {
		return jdbcTemplate.query("select * from invoice" , new BillingDtolRowMapper());
	}

	

}
