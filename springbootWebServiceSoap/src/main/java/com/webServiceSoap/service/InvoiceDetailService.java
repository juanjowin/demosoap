package com.webServiceSoap.service;

import java.util.List;

import com.webServiceSoap.dto.InvoiceDetailDto;

public interface InvoiceDetailService {
	public boolean save(InvoiceDetailDto invoiceDetailDto);
	public List<InvoiceDetailDto> findById(int iddetail);
	public boolean delete(int iddetail);
	public List<InvoiceDetailDto> findAll();
}
