package com.webServiceSoap.service;

import java.util.List;

import com.webServiceSoap.dto.BillingDto;

public interface BillingService {
	public BillingDto save(BillingDto billingDto);
	public boolean update(BillingDto billingDto);
	public boolean delete(int numinvoice);
	public BillingDto findByInvoiceNumber(int numinvoice);
	public List<BillingDto> findAll();
	
	
}
