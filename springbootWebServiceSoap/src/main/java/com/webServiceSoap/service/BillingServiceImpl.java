package com.webServiceSoap.service;


import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webServiceSoap.dao.BillingDao;
import com.webServiceSoap.dto.BillingDto;
import com.webServiceSoap.dto.InvoiceDetailDto;


@Service
public class BillingServiceImpl implements BillingService{
	
	@Autowired
	private BillingDao billingDao;
	
	

	@Override
	public BillingDto save(BillingDto billingDto) {
		
		BillingDto savebillingDto=billingDao.save(billingDto);
		try {
			if(savebillingDto==null) 
			{
				throw new ArithmeticException("ERROR al insetar registro");		
			}else {
				throw new ArithmeticException("Registro insetado con exito");
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}		
		return 	billingDao.save(savebillingDto); 
	}
	
	

	@Override
	public boolean update(BillingDto billingDto) {	
		
		boolean newbillBillingDto=billingDao.update(billingDto);
		if(newbillBillingDto==false) {
			throw new ArithmeticException("factura no encotrada... por favos ingrese numero de factura valido");
		}else if(newbillBillingDto==true){
			throw new ArithmeticException("registro modificado con exito");
		}
			return billingDao.update(billingDto);
	
	}		

	
	@Override
	public BillingDto findByInvoiceNumber(int numinvoice) {
		BillingDto numvoiceDB=billingDao.findByInvoiceNumber(numinvoice);
		 if(numinvoice<0) {
			throw new ArithmeticException("numero de factura tiene que ser un numero positivo...  por favos ingrese numero de factura valido");
		}	
	
		if(numvoiceDB==null) {
			throw new ArithmeticException("factura no encotrada... por favos ingrese numero de factura valido");
		}
		
		return billingDao.findByInvoiceNumber(numinvoice);	
	}
	
	@Override
	public List<BillingDto> findAll() {	
			return billingDao.findAll();		
	}
	
	@Override
	public boolean delete(int numinvoice) {	
		
		boolean numinvoiceDB=billingDao.delete(numinvoice);
	
		 		if(numinvoice<0) {
					throw new ArithmeticException("numero de factura tiene que ser un numero positivo...  por favos ingrese numero de factura valido");			
				}else if(numinvoiceDB==false) {
					throw new ArithmeticException("factura no encotrada.. por favos ingrese numero de factura valido");
				}
				else if(numinvoiceDB==true){
					throw new ArithmeticException("factura numero " +numinvoice + " eliminada con exito");
				}
		 		
		 		return this.billingDao.delete(numinvoice);
		 

	}
}
