package com.webServiceSoap.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webServiceSoap.dao.InvoiceDetailDao;
import com.webServiceSoap.dto.InvoiceDetailDto;
@Service
public class InvoiceDetailServiceImpl implements InvoiceDetailService {
	
	@Autowired
	private InvoiceDetailDao invoiceDetailDao;
	
	@Override
	public boolean save(InvoiceDetailDto invoiceDetailDto) {
		
		return invoiceDetailDao.save(invoiceDetailDto);
	}

	@Override
	public List<InvoiceDetailDto> findAll() {
		return invoiceDetailDao.findAll();
	}

	@Override
	public List<InvoiceDetailDto> findById(int iddetail) {
		return invoiceDetailDao.findById(iddetail);
	}

	@Override
	public boolean delete(int iddetail) {
		boolean iddetailDB=invoiceDetailDao.delete(iddetail);
		
 		if(iddetail<0) {
			throw new ArithmeticException("iddetail tiene que ser un numero positivo...  por favos ingrese numero de factura valido");			
		}else if(iddetailDB==false) {
			throw new ArithmeticException("id de detalle de factura no encotrada.. por favos ingrese numero de factura valido");
		}
		else if(iddetailDB==true){
			throw new ArithmeticException("factura con id " +iddetail + " eliminada con exito");
		}
 		
 		return this.invoiceDetailDao.delete(iddetail);
	}

	

}
