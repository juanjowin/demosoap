package com.webServiceSoap;



import static org.junit.Assert.assertNotNull;


import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;

import org.springframework.test.context.junit4.SpringRunner;

import com.webServiceSoap.AppConfig.AppConfig;
import com.webServiceSoap.dto.BillingDto;
import com.webServiceSoap.service.BillingService;


@RunWith(SpringRunner.class)
@SpringBootTest
@Import(AppConfig.class)
public class ServiceTest{

	
	@Autowired
	private BillingService billingService;

	

	@Test
	public void testfindAll() throws Exception {
      
		List<BillingDto> result = billingService.findAll();
        assertNotNull(result);
        System.out.println(result);
       
    }
	
	@Test
	public void findByInvoiceNumberTest() throws Exception{
		BillingDto result=billingService.findByInvoiceNumber(1);
		assertNotNull(result);
		System.out.println(result);
	}
	
	
	@Test
	public void saveTest() throws Exception{
		Date creationdate=new Date();
		BillingDto result=new BillingDto();
		try {
			
			result.setIdclient(1);
			result.setNuminvoice(2);
			result.setTotal(50.00); 
			result.setCreationdate(creationdate);

			BillingDto resultado2=billingService.save(result);
			
			Assert.assertTrue(true);
			System.out.println(resultado2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void updateTest() throws Exception{
		Date creationdate=new Date();
		BillingDto result=billingService.findByInvoiceNumber(1);
		try {
			
			result.setIdclient(1);
			result.setNuminvoice(1);
			result.setTotal(110); 
			result.setCreationdate(creationdate);

			boolean result2=billingService.update(result);
			
			Assert.assertTrue(true);
			System.out.println(result2);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	@Test
	public void deleteTest() {
		boolean result=billingService.delete(2);
		Assert.assertTrue(true);
		System.out.println(result);
	}
		
	

}
