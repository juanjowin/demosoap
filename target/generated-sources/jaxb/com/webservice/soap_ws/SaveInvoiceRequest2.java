//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.10.22 at 10:10:47 PM CST 
//


package com.webservice.soap_ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="invoice" type="{http://www.webServiceSoap/invoice}saveInvoiceType"/&gt;
 *         &lt;element name="product" type="{http://www.webServiceSoap/invoice}ProducType"/&gt;
 *         &lt;element name="detailInvoice" type="{http://www.webServiceSoap/invoice}InvoicedetailType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "invoice",
    "product",
    "detailInvoice"
})
@XmlRootElement(name = "saveInvoiceRequest2")
public class SaveInvoiceRequest2 {

    @XmlElement(required = true)
    protected SaveInvoiceType invoice;
    @XmlElement(required = true)
    protected ProducType product;
    @XmlElement(required = true)
    protected List<InvoicedetailType> detailInvoice;

    /**
     * Gets the value of the invoice property.
     * 
     * @return
     *     possible object is
     *     {@link SaveInvoiceType }
     *     
     */
    public SaveInvoiceType getInvoice() {
        return invoice;
    }

    /**
     * Sets the value of the invoice property.
     * 
     * @param value
     *     allowed object is
     *     {@link SaveInvoiceType }
     *     
     */
    public void setInvoice(SaveInvoiceType value) {
        this.invoice = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link ProducType }
     *     
     */
    public ProducType getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProducType }
     *     
     */
    public void setProduct(ProducType value) {
        this.product = value;
    }

    /**
     * Gets the value of the detailInvoice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the detailInvoice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDetailInvoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InvoicedetailType }
     * 
     * 
     */
    public List<InvoicedetailType> getDetailInvoice() {
        if (detailInvoice == null) {
            detailInvoice = new ArrayList<InvoicedetailType>();
        }
        return this.detailInvoice;
    }

}
